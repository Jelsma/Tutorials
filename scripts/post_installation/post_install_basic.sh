
#!/bin/bash

# Settings
ANACONDA='Anaconda3-2021.11-Linux-x86_64.sh'

# Defining Colors for printing
BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BROWNORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color


printf "\n\n${GREEN}[!]: Executing post install script...\n${NC}"

# Updating and upgrading the system
printf "\n${YELLOW}[!]: Updating system...\n${NC}"
printf "\n${CYAN}[!]: Running apt update...\n${NC}"
sudo apt update -y
printf "\n${CYAN}[!]: Running apt upgrade...\n${NC}"
sudo apt upgrade -y

# Installing OpenSSH Server
printf "\n${YELLOW}[!]: Installing OpenSSH...\n${NC}"
sudo apt install openssh-server -y

# Installing CURL
printf "\n${YELLOW}[!]: Installing CURL...\n${NC}"
sudo apt install curl -y

# cifs
printf "\n${YELLOW}[!]: Installing cifs-utils...\n${NC}"
sudo apt-get install cifs-utils -y

# Installing Screen
printf "\n${YELLOW}[!]: Installing Screen...\n${NC}"
sudo apt install screen -y

# Install Tmux
printf "\n${YELLOW}[!]: Installing Tmux... \n${NC}"
sudo apt install tmux -y

# Install Rsync
printf "\n${YELLOW}[!]: Installing Rsync... \n${NC}"
sudo apt install rsync -y

# Installing HTOP
printf "\n${YELLOW}[!]: Installing HTOP...\n${NC}"
sudo apt install htop -y

# Installing Unzip
printf "\n${YELLOW}[!]: Installing unzip...\n${NC}"
sudo apt install unzip -y

# Installing git
printf "\n${YELLOW}[!]: Installing git...\n${NC}"
sudo apt install git -y

# Installing anaconda
printf "\n${YELLOW}[!]: Installing Anaconda...\n${NC}"
cd /tmp
curl -O https://repo.anaconda.com/archive/$ANACONDA
bash $ANACONDA -b
printf "\n${CYAN}[!]: Initializing Anaconda...\n${NC}"
source ~/.bashrc
source ~/anaconda3/bin/activate
conda init
printf "\n${CYAN}[!]: Updating Anaconda...\n${NC}"
conda update conda -y
conda update anaconda -y

# Installing Docker
printf "\n${YELLOW}[!]: Installing Docker...\n${NC}"
printf "\n${CYAN}[!]: Uninstalling old docker versions... \n${NC}"
sudo apt-get remove docker docker-engine docker.io containerd runc
printf "\n${CYAN}[!]: Setting up the repository... \n${NC}"
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
printf "\n${CYAN}[!]: Installing Docker engine... \n${NC}"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
printf "\n${CYAN}[!]: Verifing Docker is correctly installed... \n${NC}"
sudo docker run hello-world
printf "\n${CYAN}[!]: Setting up Docker permissions \n${NC}"
sudo groupadd docker
sudo usermod -aG docker $USER

# Installing Docker Compose
printf "\n${YELLOW}[!]: Installing Docker Compose...\n${NC}"
sudo apt-get --no-install-recommends install -y python3-pip python3-setuptools
sudo python3 -m pip install setuptools docker-compose

# End of Post Install
printf "\n${GREEN}[!]: Post Install Completed!\n${NC}"
printf "\n\n\n${RED}Please perform the following actions:\n${NC}"
printf "${RED}    1. Reboot the system\n${NC}"
printf "${RED}    2. Restart your terminal to activate conda!\n${NC}"
printf "\n\n\n"

