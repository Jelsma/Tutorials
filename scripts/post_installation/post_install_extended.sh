#!/bin/bash

# Defining Colors for printing
BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BROWNORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color

printf "\n\n${GREEN}[!]: Executing extended post install script...\n${NC}"

# Updating and upgrading the system
printf "\n${YELLOW}[!]: Updating system...\n${NC}"
printf "\n${CYAN}[!]: Running apt update...\n${NC}"
sudo apt update -y
printf "\n${CYAN}[!]: Running apt upgrade...\n${NC}"
sudo apt upgrade -y

# Installing snapd
printf "\n${YELLOW}[!]: Installing snapd (Package Manager) ...\n${NC}"
sudo apt install snapd

# Install l2tp vpn
printf "\n${YELLOW}[!]: Installing network-manager-l2tp (L2TP Network option for VPN) ...\n${NC}"
sudo apt install network-manager-l2tp

# Installing Sublime Text
printf "\n${YELLOW}[!]: Installing Sublime Text (Text Editor) ...\n${NC}"
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt update
sudo apt install sublime-text

# Installing Gimp
printf "\n${YELLOW}[!]: Installing Gimp (Photoshop Tool) ...\n${NC}"
sudo apt install gimp -y

# Installing 1Password
printf "\n${YELLOW}[!]: Installing 1Password (Password Manager) ...\n${NC}"
curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' | sudo tee /etc/apt/sources.list.d/1password.list
sudo mkdir -p /etc/debsig/policies/AC2D62742012EA22/
curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
sudo mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
sudo apt update && sudo apt install 1password

# Installing KeePassXC
printf "\n${YELLOW}[!]: Installing KeepPassXC (Password Manager) ...\n${NC}"
sudo snap install keepassxc

# Installing FreeCAD
printf "\n${YELLOW}[!]: Installing FreeCAD (3D Modeling Software) ...\n${NC}"
sudo apt install freecad -y

# Installing Cura
printf "\n${YELLOW}[!]: Installing Cura (3D Printing Slicer) ...\n${NC}"
sudo snap install cura-slicer

# Installing Discord
printf "\n${YELLOW}[!]: Installing Discord (Chat app) ...\n${NC}"
sudo snap install discord

# Installing PyCharm CE
printf "\n${YELLOW}[!]: Installing Pycharm CE (Python Editor and Debugger) ...\n${NC}"
sudo snap install pycharm-community --classic

# Installing Open Office
printf "\n${YELLOW}[!]: Installing LibreOffice (Office Software)...\n${NC}"
sudo apt install libreoffice -y

# Installing KiCAD
#printf "\n${YELLOW}[!]: Installing KiCAD...\n${NC}"

# Removing old installations
#sudo apt-get purge kicad kicad-symbols kicad-footprints kicad-packages3d kicad-templates -y
#sudo apt clean -y
#sudo apt autoremove -y
#sudo apt -f install -y
#sudo apt update

# Install specific version
#sudo add-apt-repository --yes ppa:kicad/kicad-5.1-releases
#sudo apt-cache madison kicad
#sudo apt install --install-recommends kicad=x -y

# install latest version
#sudo apt install --install-recommends kicad -y

#sudo apt install kicad-packages3d
#sudo apt-mark hold kicad

# End of Post Install
printf "\n${GREEN}[!]: Extended Post Install Completed!\n${NC}"
printf "\n\n\n"

