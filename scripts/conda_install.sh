#!/bin/bash

# Settings
ANACONDA='Anaconda3-20.02-Linux-armv7l.sh'

# Installing anaconda
printf "\n${YELLOW}[!]: Installing Anaconda...\n${NC}"
cd /tmp
curl -O https://repo.anaconda.com/archive/$ANACONDA
bash $ANACONDA -b
printf "\n${CYAN}[!]: Initializing Anaconda...\n${NC}"
source ~/.bashrc
source ~/anaconda3/bin/activate
conda init
printf "\n${CYAN}[!]: Updating Anaconda...\n${NC}"
conda update conda -y
conda update anaconda -y

# End of Post Install
printf "\n${GREEN}[!]: Install completed! Restart your terminal for the changes to take effect.\n${NC}"
