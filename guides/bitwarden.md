

# Setting up a secure Mosquitto MQTT Broker
This is a guide on how to setup a MQTT Broker with authentication, and TLS/SSL certificates. This broker can be externally accesed all around the world. 

## 1. Setting up a Mosquitto MQTT Broker

 **2. Installing Mosquitto Broker + client**
 Installing the Mosquitto broker is as simple as installing it from the apt-respository:
```
sudo apt-get update
sudo apt install mosquitto
sudo apt install mosquitto-clients
```
Mosquitto is already configured as a systemctl service, and should start during boot.

**3. Testing broker functionality (optional)**
To test if the broker is functioning properly, we can subscribe to a test topic:
Subscribe to the test topic:
```
mosquitto_sub -t "test"
```

To publish a message on this topic, we can login to a second instance of the terminal:
```
mosquitto_pub -m "Hello World!" -t "test"
```

If everything is working proberly, the message "Hello World!" should appear in the the other terminal!

**4. Authentication**
Since the broker is not secured in any way, everyone can join the broker. It is recommended to add authentication to your broker, even if it is in your own network.

To add a user,, we need to generate a password file. (the password will be encrypted and cannot be retrieved later.)
```
sudo mosquitto_passwd -c /etc/mosquitto/passwd USERNAME
Password: PASSWORD
```

Edit the Mosquitto configuration file:
```sudo nano /etc/mosquitto/conf.d/default.conf```

Paste the following in the file. This will block anyone without the correct credentials to connect to your broker, and point to the password file. 
```
allow_anonymous false
password_file /etc/mosquitto/passwd
```
Save and exit the text editor and restart the Mosquitto service. 
```sudo systemctl restart mosquitto```

**5. Testing if broker authentication is properly configured (optional)**
We want to test if the authentication is functioning properly and no unauthenticated users can connect to broker. 

Subscribe to the test topic once again.
```mosquitto_sub -t "test" -u "USERNAME" -P "PASSWORD"```

Open a new terminal instance and try to send a message. (which SHOULD fail!)
```mosquitto_pub -t "test" -m "Hello World!"```

If configured correctly, you should receive an error:
```
Connection Refused: not authorised.
Error: The connection was refused.
```
Now try to send the message again, with the proper credentials:
```mosquitto_pub -t "test" -m "Hello World!" -u "USERNAME" -P "PASSWORD"```

The message should appear in the other window. Congratulations! You have configured a MQTT broker with authentication. 

However, if you want to make the broker available outside you local network, you should configure TLS/SSL before opening any ports in your router!

## 2. Configuring TLS/SSL certificates
TLS/SSL certificates enables you to remotely access your MQTT broker in a secure way. 

**1. Create and enter the directory where the certificates will be stored**
First, go to your user folder and create a new directory where the certificates and keys will be stored in and open the directory::
```
cd
```
```
sudo mkdir mosquitto-certs
```
```
cd mosquitto-certs
```

**2.  Create a client private key**
```sudo openssl genrsa -out mosq-ca.key 2048```

***BEWARE!**
When configuring the certificate, the Common Name (CN) MUST be equal to your broker domain (e.g. broker.example.com)*


**3. Create a client certificate (using the private key)**
```
sudo openssl req -new -x509 -days 3650 -key mosq-ca.key -out mosq-ca.crt
```

**4. Create a server key** 
```
sudo openssl genrsa -out mosq-serv.key 2048
```
***BEWARE!**
When configuring the certificate, the Common Name (CN) MUST be equal to your broker domain (e.g. broker.example.com)*

**5. Create a Certificate Signing Request (CSR) for the server certificate**
```
sudo openssl req -new -key mosq-serv.key -out mosq-serv.csr
```

**5. Create a server certificate**
```
sudo openssl x509 -req -in mosq-serv.csr -CA mosq-ca.crt -CAkey mosq-ca.key -CAcreateserial -out mosq-serv.crt -days 3650 -sha256
```

**6. Configure Mosquitto**

Get the current working directory (where the certificates are placed)
```pwd```

The output is something similar to:
```/home/USERNAME/mosquitto-certs```

Edit the Mosquitto configuration file:
```sudo nano /etc/mosquitto/mosquitto.conf```

Add these lines to the config (use your OWN path!)
```
listener 8883
cafile /home/USERNAME/mosquitto-certs/mosq-ca.crt
certfile /home/USERNAME/mosquitto-certs/mosq-serv.crt
keyfile /home/USERNAME/mosquitto-certs/mosq-serv.key
require_certificate true
```

**7. Restart Mosquitto**
```sudo systemctl restart mosquitto```

## 3. Connecting to broker outside LAN 
**1. Opening ports in your router**
To make the broker accesable outside your local network, a port needs to be opened in your router. ONLY do this if you have configured a secure broker with TLS/SSL certificates! 

In your router open port:
8883 (TCP)

Set the port as TCP only, UDP packets can be blocked since they are not used by MQTT.

**2. Retrieving the certificates and keys**
Copy the following files from your MQTT server and keep them in a secure location on your PC or NAS:

 - mosq-serv.crt (Server certificate )
 - mosq-ca.crt (Client certificate)
 - mosq-ca.key (Client private key)

**3. Connecting to the broker**
You should now be able to connect to your broker securely. To connect to your broker you need to provide to following details:

 - Broker: Your broker domain (Same specified in Common Name for keys) 
 - Username: MQTT Username
 - Password: MQTT Password
 - Server certificate: ```mosq-serv.crt```
 - Client certificate: ```mosq-ca.crt```
 - Client key: ```mosq-ca.key```


## References
Information about install Mosquitto can be found  [here.](https://www.vultr.com/docs/how-to-install-mosquitto-mqtt-broker-server-on-ubuntu-16-04/)
Information about configuring TLS/SSL can be found [here.](https://dzone.com/articles/mqtt-security-securing-a-mosquitto-server)
